#!/usr/bin/env bash 


git submodule add git://git.wincent.com/command-t.git vim/bundle/command-t 
git submodule add https://github.com/vim-scripts/FuzzyFinder.git vim/bundle/FuzzyFinder
git submodule add https://github.com/vim-scripts/abolish.vim.git vim/bundle/abolish.vim
git submodule add https://github.com/scrooloose/nerdtree.git vim/bundle/nerdtree
git submodule add https://github.com/plasticboy/vim-markdown.git vim/bundle/vim-markdown
git submodule add https://github.com/vim-scripts/L9.git vim/bundle/L9
git submodule add https://github.com/vim-scripts/ctrlp.vim.git vim/bundle/ctrlp.vim
git submodule add https://github.com/vim-scripts/ucompleteme.git vim/bundle/ucompleteme



